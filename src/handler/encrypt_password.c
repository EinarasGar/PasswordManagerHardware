#include <stdint.h>  // uint*_t

#include "encrypt_password.h"
#include "../constants.h"
#include "../globals.h"
#include "../io.h"
#include "../sw.h"
#include "../types.h"
#include "common/buffer.h"
#include "os.h"
#include "lcx_aes.h"
#include "../ui/confirm_action.h"
#include "../ui/display_password.h"


int handler_encrypt_password(buffer_t *cdata, bool decrypt, bool displayOnScreen) {
    int input_len_padded = ((cdata->size-1)/16+1)*16;
    uint8_t inputBytes[input_len_padded];
    explicit_bzero(inputBytes, sizeof(inputBytes));
    buffer_copy(cdata, inputBytes, cdata->size);

    perform_operation(inputBytes,input_len_padded,decrypt);

    if (decrypt && displayOnScreen) {
        send_response(SW_DISPLAY);
        ui_display_password();
        // explicit_bzero(&G_password_context, sizeof(G_password_context));
        return 1;
    }    

    ui_confirm_action();
    return 1;
}

void perform_operation(uint8_t *inArray, int input_len_padded, int decrypt) {

    uint8_t outputBytes[input_len_padded];
    explicit_bzero(outputBytes, sizeof(outputBytes));

    unsigned int path[2] = {-2147483604, -2147483647};
    uint8_t chainCode[32] = {3, 34, 200, 246, 129, 231, 39, 78, 118, 124, 238, 9, 184, 228, 23, 112, 230, 210, 175, 213, 4, 253, 95, 133, 223, 170, 179, 225, 255, 108, 223, 204};

    uint8_t raw_private_key[32] = {0};
    os_perso_derive_node_bip32(CX_CURVE_256K1,
                             path,
                             2,
                             raw_private_key,
                             chainCode);

    cx_aes_key_t aes_key;
    cx_aes_init_key_no_throw(raw_private_key,32,&aes_key);

    int mode = decrypt ? CX_DECRYPT : CX_ENCRYPT;
    cx_aes(&aes_key,mode,inArray,input_len_padded,outputBytes,input_len_padded);

    PRINTF("INPUT: \n");
    for(int i = 0 ; i < input_len_padded; i ++){
        PRINTF("%d ",inArray[i]);
    }
    PRINTF("\nOUTPUT: \n");
    for(int i = 0 ; i < input_len_padded; i ++){
        PRINTF("%d ",outputBytes[i]);
    }
    PRINTF("\n");

    explicit_bzero(raw_private_key, sizeof(raw_private_key));

    int size = decrypt ? strlen((char*)outputBytes) : input_len_padded;
   

    G_password_context.size = size;
    memcpy(&G_password_context.passwordBytes, &outputBytes, size);
}

int send_response(uint16_t sw){
    if(sw == SW_DENY){
        explicit_bzero(&G_password_context, sizeof(G_password_context));
    }
    if(sw == SW_DISPLAY){
        uint8_t emptyArray[0];
        buffer_t rdata = {.ptr = emptyArray, .size = 0, .offset = 0};
        return io_send_response(&rdata, sw);
    }
    buffer_t rdata = {.ptr = G_password_context.passwordBytes, .size = G_password_context.size, .offset = 0};
    return io_send_response(&rdata, sw);
}