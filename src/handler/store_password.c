#include <stdint.h>  // uint*_t

#include "encrypt_password.h"
#include "../constants.h"
#include "../globals.h"
#include "../io.h"
#include "../sw.h"
#include "../types.h"
#include "common/buffer.h"
#include "os.h"
#include "lcx_aes.h"
#include "../ui/confirm_action.h"
#include "../ui/display_password.h"
#include "encrypt_password.h"


int handler_store_password(buffer_t *cdata) {
    uint8_t passwordSize;    
    buffer_read_u8(cdata, &passwordSize);
    PRINTF("Password size: %d\n", passwordSize);
    uint8_t passwordBytes[passwordSize];
    explicit_bzero(passwordBytes, sizeof(passwordBytes));
    for(int i = 0 ; i < passwordSize; i ++){
        buffer_read_u8(cdata, &passwordBytes[i]);
    }

    uint8_t nameSize;    
    buffer_read_u8(cdata, &nameSize);
    uint8_t nameBytes[nameSize];
    explicit_bzero(nameBytes, sizeof(nameBytes));
    for(int i = 0 ; i < nameSize; i ++){
        buffer_read_u8(cdata, &nameBytes[i]);
    }

    uint8_t websiteSize;    
    buffer_read_u8(cdata, &websiteSize);
    uint8_t websiteBytes[websiteSize];
    explicit_bzero(websiteBytes, sizeof(websiteBytes));
    for(int i = 0 ; i < websiteSize; i ++){
        buffer_read_u8(cdata, &websiteBytes[i]);
    }

    PRINTF("Password: %d ", passwordSize);
    for(int i = 0; i < passwordSize; i++){
        PRINTF("%d ", passwordBytes[i]);
    }
    PRINTF("\n");
    PRINTF("Name: %d ", nameSize);
    for(int i = 0; i < nameSize; i++){
        PRINTF("%d ", nameBytes[i]);
    }
    PRINTF("\n");
    PRINTF("Website: %d ", websiteSize);
    for(int i = 0; i < websiteSize; i++){
        PRINTF("%d ", websiteBytes[i]);
    }
    PRINTF("\n");

    uint8_t emptyArray[0];
    buffer_t rdata = {.ptr = emptyArray, .size = 0, .offset = 0};

    if(N_savedPasswordsStorage.size >= MAX_SAVED_PASSWORDS)
        return io_send_response(&rdata, SW_NO_STORAGE);


    savedPasswordsStorage_t passwordStorage = N_savedPasswordsStorage;
    strlcpy(passwordStorage.passwords[passwordStorage.size].website, websiteBytes, websiteSize+1);
    strlcpy(passwordStorage.passwords[passwordStorage.size].username, nameBytes, nameSize+1);


    int input_len_padded = ((passwordSize-1)/16+1)*16;
    perform_operation(passwordBytes, input_len_padded, 0);

    memcpy(passwordStorage.passwords[passwordStorage.size].password.passwordBytes, G_password_context.passwordBytes, G_password_context.size);
    passwordStorage.passwords[passwordStorage.size].password.size = G_password_context.size;
    passwordStorage.size++;

    nvm_write((void *) &N_savedPasswordsStorage, (void *) &passwordStorage, sizeof(savedPasswordsStorage_t));


    return io_send_response(&rdata, SW_OK);
}