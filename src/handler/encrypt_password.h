#pragma once

#include "os.h"
#include "../common/buffer.h"


/**
 * Handler for ENCRYPT_PASSWORD command. Send APDU response with ASCII
 * encoded encrypted password.
 *
 * @see variable APPNAME in Makefile.
 *
 * @return zero or positive integer if success, negative integer otherwise.
 *
 */
int handler_encrypt_password(buffer_t *cdata, bool decrypt, bool displayOnScreen);

void perform_operation(uint8_t *inArray, int size, int decrypt);


int send_response(uint16_t sw);