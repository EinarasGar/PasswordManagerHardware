#pragma once

#include <stddef.h>  // size_t
#include <stdint.h>  // uint*_t

#include "constants.h"
#include "common/bip32.h"

/**
 * Enumeration for the status of IO.
 */
typedef enum {
    READY,     /// ready for new event
    RECEIVED,  /// data received
    WAITING    /// waiting
} io_state_e;

/**
 * Enumeration with expected INS of APDU commands.
 */
typedef enum {
    GET_VERSION = 0x03,     /// version of the application
    GET_APP_NAME = 0x04,    /// name of the application
    GET_PUBLIC_KEY = 0x05,  /// public key of corresponding BIP32 path
    SIGN_TX = 0x06,          /// sign transaction with BIP32 path
    ENCRYPT_PASSWORD = 0x07,
    STORE_PASSWORD = 0x08,
} command_e;

/**
 * Structure with fields of APDU command.
 */
typedef struct {
    uint8_t cla;    /// Instruction class
    command_e ins;  /// Instruction code
    uint8_t p1;     /// Instruction parameter 1
    uint8_t p2;     /// Instruction parameter 2
    uint8_t lc;     /// Lenght of command data
    uint8_t *data;  /// Command data
} command_t;

/**
 * Enumeration with parsing state.
 */
typedef enum {
    STATE_NONE,     /// No state
    STATE_PARSED,   /// Transaction data parsed
    STATE_APPROVED  /// Transaction data approved
} state_e;

/**
 * Structure for global context.
 */
typedef struct {
    uint8_t size;
    uint8_t passwordBytes[MAX_PASSWORD_LENGTH];
} global_passwordctx_t;

typedef struct internalStorage_t {
    unsigned char confirmAction;
    uint8_t initialized;
} internalStorage_t;

typedef struct savedPassword_t {
    char website[MAX_PASSWORD_LENGTH];
    char username[MAX_PASSWORD_LENGTH];
    global_passwordctx_t password;
} savedPassword_t;

typedef struct savedPasswordsStorage_t {
    uint8_t size;
    savedPassword_t passwords[MAX_SAVED_PASSWORDS];
    uint8_t initialized;
} savedPasswordsStorage_t;

enum e_state {
   STATIC_SCREEN,
   DYNAMIC_SCREEN,
};

typedef struct {
    char confirmSettingString[12];
    char passwordText[MAX_PASSWORD_LENGTH];
    char storedPasswordWebsite[MAX_PASSWORD_LENGTH];
    char storedPasswordName[MAX_PASSWORD_LENGTH];
    char howManyStoredString[12];
    enum e_state current_state;
    uint8_t storedPasswordIndex;
} display_strings_t;

