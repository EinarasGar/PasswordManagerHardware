/*****************************************************************************
 *   Ledger App Boilerplate.
 *   (c) 2020 Ledger SAS.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *****************************************************************************/

#include "os.h"
#include "ux.h"
#include "glyphs.h"

#include "../globals.h"
#include "menu.h"
#include "stored_password.h"

UX_STEP_NOCB(ux_menu_ready_step, pnn, {&C_padlock_logo, "Password", "Manager ready"});
UX_STEP_CB(ux_menu_stored_step, pb, ui_view_stored_passwords(), {&C_icon_eye, "Stored Passwords"});
UX_STEP_CB(ux_menu_about_step, pb, ui_menu_about(), {&C_icon_certificate, "About"});
UX_STEP_CB(ux_menu_settings_step, pb, ui_settings_menu(), {&C_icon_coggle, "Settings"});
UX_STEP_VALID(ux_menu_exit_step, pb, os_sched_exit(-1), {&C_icon_dashboard_x, "Quit"});

// FLOW for the main menu:
// #1 screen: ready
// #2 screen: version of the app
// #3 screen: about submenu
// #4 screen: quit
UX_FLOW(ux_menu_main_flow,
        &ux_menu_ready_step,
        &ux_menu_stored_step,
        &ux_menu_about_step,
        &ux_menu_settings_step,
        &ux_menu_exit_step,
        FLOW_LOOP);

void ui_menu_main() {
    if (G_ux.stack_count == 0) {
        ux_stack_push();
    }

    ux_flow_init(0, ux_menu_main_flow, NULL);
}

UX_STEP_NOCB(ux_menu_info_step, bn, {"Password Manager", "Final Year Project"});
UX_STEP_NOCB(ux_menu_name_step, bn, {"2022", "Einaras Garbasauskas"});
UX_STEP_NOCB(ux_menu_version_step, bn, {"Version", APPVERSION});
UX_STEP_CB(ux_menu_back_step, pb, ui_menu_main(), {&C_icon_back, "Back"});

// FLOW for the about submenu:
// #1 screen: app info
// #2 screen: back button to main menu
UX_FLOW(ux_menu_about_flow, &ux_menu_info_step, &ux_menu_name_step, &ux_menu_version_step, &ux_menu_back_step, FLOW_LOOP);

void ui_menu_about() {
    ux_flow_init(0, ux_menu_about_flow, NULL);
}

char* textas = "text";
UX_STEP_CB(ux_menu_settings_info_step, bn, ui_settings_toggle_confirm(),   {"Confirm action",G_display_strings.confirmSettingString});

// FLOW for the about submenu:
// #1 screen: app info
// #2 screen: back button to main menu
UX_FLOW(ux_menu_settings_flow, &ux_menu_settings_info_step, &ux_menu_back_step, FLOW_LOOP);

void ui_settings_menu() {
    strlcpy(G_display_strings.confirmSettingString, (N_storage.confirmAction ? "Enabled" : "NOT Enabled"), 12);
    ux_flow_init(0, ux_menu_settings_flow, NULL);
}


void ui_settings_toggle_confirm() {
    int switchedValue = (N_storage.confirmAction ? 0 : 1);
    nvm_write((void*) &N_storage.confirmAction, (void*) &switchedValue, sizeof(uint8_t));
    ui_settings_menu();
}