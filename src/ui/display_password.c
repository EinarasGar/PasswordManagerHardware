#include "os.h"
#include "ux.h"
#include "glyphs.h"

#include "../globals.h"
#include "menu.h"
#include "display_password.h"
#include "../handler/encrypt_password.h"


UX_STEP_CB(ux_display_password, bn_paging, ui_display_password_close(),   {"Password",G_display_strings.passwordText});

UX_FLOW(ux_display_password_flow, &ux_display_password, FLOW_LOOP);

void ui_display_password(){
    strlcpy(G_display_strings.passwordText, (char*)&G_password_context.passwordBytes, G_password_context.size+1);

    ux_flow_init(0, ux_display_password_flow, NULL);
}

void ui_display_password_close(){
    ui_menu_main();
}