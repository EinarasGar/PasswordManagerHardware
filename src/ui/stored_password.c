#include "os.h"
#include "ux.h"
#include "glyphs.h"

#include "../globals.h"
#include "menu.h"
#include "stored_password.h"
#include "display_password.h"
#include "../handler/encrypt_password.h"
#include "../sw.h"
#include "../types.h"


UX_STEP_NOCB(ux_sot, bn, {"Currently stored:",G_display_strings.howManyStoredString});
UX_STEP_CB(ux_back_step, pb, ui_menu_main(), {&C_icon_back, "Back"});
UX_STEP_CB(ux_erase, pb, ui_stored_passwords_erase(), {&C_icon_crossmark, "Erase passwords"});


// UX_FLOW(ux_stored_passwords_flow, &ux_sot, &ux_back_step, FLOW_LOOP);

UX_STEP_INIT(
   step_upper_delimiter,
   NULL,
   NULL,
   {
      // This function will be detailed later on.
      display_next_state(true);
   }
);

UX_STEP_CB(
   step_generic,
   bnnn_paging,
   select_password(),
   {
      .title = G_display_strings.storedPasswordWebsite,
      .text = G_display_strings.storedPasswordName,
   }
);

// Note we're using UX_STEP_INIT because this step won't display anything.
UX_STEP_INIT(
   step_lower_delimiter,
   NULL,
   NULL,
   {
      // This function will be detailed later on.
      display_next_state(false);
   }
);


UX_FLOW(dynamic_flow,
         &ux_sot,

         &step_upper_delimiter, // A special step that serves as the upper delimiter. It won't print anything on the screen.
         &step_generic, // The generic step that will actually display stuff on the screen.
         &step_lower_delimiter, // A special step that serves as the lower delimiter. It won't print anything on the screen.

         &ux_back_step,
         &ux_erase,
         FLOW_LOOP
   );

UX_FLOW(empty_flow,
         &ux_sot,
         &ux_back_step,
         &ux_erase,
         FLOW_LOOP
   );

void bnnn_paging_edgecase() {
    G_ux.flow_stack[G_ux.stack_count - 1].prev_index = G_ux.flow_stack[G_ux.stack_count - 1].index - 2;
    G_ux.flow_stack[G_ux.stack_count - 1].index--;
    ux_flow_relayout();
}

void display_next_state(bool is_upper_delimiter) {
    if (is_upper_delimiter) { 
        // We're called from the upper delimiter.
        if (G_display_strings.current_state == STATIC_SCREEN) {
            // Fetch new data.
            bool dynamic_data = get_next_data(true, true);

            if (dynamic_data) {
                // We found some data to display so we now enter in dynamic mode.
                G_display_strings.current_state = DYNAMIC_SCREEN;
            }

            // Move to the next step, which will display the screen.
            ux_flow_next();
        } else {
            // The previous screen was NOT a static screen, so we were already in a dynamic screen.

            // Fetch new data.
            bool dynamic_data = get_next_data(true, false);
            if (dynamic_data) {
                // We found some data so simply display it.
                ux_flow_next();
            }
            else {
                // There's no more dynamic data to display, so
                // update the current state accordingly.
                G_display_strings.current_state = STATIC_SCREEN;

                // Display the previous screen which should be a static one.
                ux_flow_prev();
            }
        } 
    }
    else {
        if (G_display_strings.current_state == STATIC_SCREEN) {
            // // Fetch new data.
            bool dynamic_data = get_next_data(false,true);

            if (dynamic_data) {
                // We found some data to display so enter in dynamic mode.
                G_display_strings.current_state = DYNAMIC_SCREEN;
            }

            // Display the data.
            ux_flow_prev();
        } else {
            // We're being called from a dynamic screen, so the user was already browsing the array.

            // Fetch new data.
            bool dynamic_data = get_next_data(false,false);
            if (dynamic_data) {
                // We found some data, so display it.
                // Similar to `ux_flow_prev()` but updates layout to account for `bnnn_paging`'s weird behaviour.
                bnnn_paging_edgecase();
            } else {
                // We found no data so make sure we update the state accordingly.
                G_display_strings.current_state = STATIC_SCREEN;

                // Display the next screen
                ux_flow_next();
            }
        }
    }
}

bool get_next_data(bool is_upper_delimiter, bool wasStatic) {   
    if(is_upper_delimiter && wasStatic){
        G_display_strings.storedPasswordIndex = 0;
    }
    if(is_upper_delimiter && wasStatic == false){
        G_display_strings.storedPasswordIndex--;

        if(G_display_strings.storedPasswordIndex == 255)
            return false;
    }
    if(is_upper_delimiter == false && wasStatic){
        G_display_strings.storedPasswordIndex = N_savedPasswordsStorage.size - 1;
    }
    if(is_upper_delimiter == false && wasStatic == false){
        
        G_display_strings.storedPasswordIndex++;

        if(G_display_strings.storedPasswordIndex == N_savedPasswordsStorage.size)
            return false;
         
    }
    uint8_t currentIndex = G_display_strings.storedPasswordIndex;

    strlcpy(G_display_strings.storedPasswordWebsite, N_savedPasswordsStorage.passwords[currentIndex].website, 17);
    strlcpy(G_display_strings.storedPasswordName, N_savedPasswordsStorage.passwords[currentIndex].username, 18);
    return true;
}

void ui_view_stored_passwords(){
    char str[3];
    SPRINTF(str, "%d", N_savedPasswordsStorage.size);

    strlcpy(G_display_strings.howManyStoredString, str, 3);
    G_display_strings.storedPasswordIndex = 0;
    G_display_strings.current_state = STATIC_SCREEN;

    if(N_savedPasswordsStorage.size != 0)
        ux_flow_init(0, dynamic_flow, NULL);
    else
        ux_flow_init(0, empty_flow, NULL);
}

void select_password(){
    perform_operation(N_savedPasswordsStorage.passwords[G_display_strings.storedPasswordIndex].password.passwordBytes, N_savedPasswordsStorage.passwords[G_display_strings.storedPasswordIndex].password.size, 1);
    ui_display_password();
    explicit_bzero(&G_password_context, sizeof(G_password_context));
}

void ui_stored_passwords_erase(void){
    savedPasswordsStorage_t passwordStorage;
    explicit_bzero(&passwordStorage, sizeof(passwordStorage));
    passwordStorage.initialized = 0x01;
    nvm_write((void *) &N_savedPasswordsStorage, (void *) &passwordStorage, sizeof(savedPasswordsStorage_t));
    ui_menu_main();
}
