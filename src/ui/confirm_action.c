#include "os.h"
#include "ux.h"
#include "glyphs.h"

#include "../globals.h"
#include "menu.h"
#include "confirm_action.h"
#include "../handler/encrypt_password.h"
#include "../sw.h"


UX_STEP_CB(ux_confirm_action_accept, nn, ui_confirm_action_accept_callback(),   {"Accept pasword","decryption"});
UX_STEP_CB(ux_confirm_action_deny, nn, ui_confirm_action_deny_callback(),   {"Deny password","decryption"});

UX_FLOW(ux_confirm_action_flow, &ux_confirm_action_accept, &ux_confirm_action_deny, FLOW_LOOP);

void ui_confirm_action(){
    if(N_storage.confirmAction){
        ux_flow_init(0, ux_confirm_action_flow, NULL);
    } else {
        send_response(SW_OK);
    }
}

void ui_confirm_action_accept_callback(){
    send_response(SW_OK);
    ui_menu_main();
}

void ui_confirm_action_deny_callback(){
    send_response(SW_DENY);
    ui_menu_main();
}