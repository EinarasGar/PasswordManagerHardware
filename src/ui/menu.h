#pragma once

/**
 * Show main menu (ready screen, version, about, quit).
 */
void ui_menu_main(void);

/**
 * Show about submenu (copyright, date).
 */
void ui_menu_about(void);

void ui_settings_menu(void);

void ui_settings_toggle_confirm(void);
