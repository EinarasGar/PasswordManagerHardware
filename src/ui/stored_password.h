#pragma once

void ui_view_stored_passwords(void);
void display_next_state(bool is_upper_delimiter);
void select_password(void);
void ui_stored_passwords_erase(void);
bool get_next_data(bool is_upper_delimiter, bool isNext);
