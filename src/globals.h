#pragma once

#define N_storage (*(volatile internalStorage_t *) PIC(&N_storage_real))
#define N_savedPasswordsStorage (*(volatile savedPasswordsStorage_t *) PIC(&N_savedPasswordsStorage_real))


#include <stdint.h>

#include "ux.h"

#include "io.h"
#include "types.h"
#include "constants.h"

/**
 * Global buffer for interactions between SE and MCU.
 */
extern uint8_t G_io_seproxyhal_spi_buffer[IO_SEPROXYHAL_BUFFER_SIZE_B];

/**
 * Global variable with the lenght of APDU response to send back.
 */
extern uint32_t G_output_len;

/**
 * Global structure to perform asynchronous UX aside IO operations.
 */
extern ux_state_t G_ux;

/**
 * Global structure with the parameters to exchange with the BOLOS UX application.
 */
extern bolos_ux_params_t G_ux_params;

/**
 * Global enumeration with the state of IO (READY, RECEIVING, WAITING).
 */
extern io_state_e G_io_state;


extern const internalStorage_t N_storage_real;

extern const savedPasswordsStorage_t N_savedPasswordsStorage_real;

extern global_passwordctx_t G_password_context;

extern display_strings_t G_display_strings;