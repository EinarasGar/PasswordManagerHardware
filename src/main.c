/*****************************************************************************
 *   Ledger App Boilerplate.
 *   (c) 2020 Ledger SAS.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *****************************************************************************/

#include <stdint.h>  // uint*_t
#include <string.h>  // memset, explicit_bzero

#include "os.h"
#include "ux.h"

#include "types.h"
#include "globals.h"
#include "io.h"
#include "sw.h"
#include "ui/menu.h"
#include "apdu/parser.h"
#include "apdu/dispatcher.h"

uint8_t G_io_seproxyhal_spi_buffer[IO_SEPROXYHAL_BUFFER_SIZE_B];
io_state_e G_io_state;
ux_state_t G_ux;
bolos_ux_params_t G_ux_params;
global_passwordctx_t G_password_context;
display_strings_t G_display_strings;

const internalStorage_t N_storage_real;
const savedPasswordsStorage_t N_savedPasswordsStorage_real;

/**
 * Handle APDU command received and send back APDU response using handlers.
 */
void app_main() {
    // Length of APDU command received in G_io_apdu_buffer
    int input_len = 0;
    // Structured APDU command
    command_t cmd;

    // Reset length of APDU response
    G_output_len = 0;
    G_io_state = READY;

    // Reset context
    explicit_bzero(&G_password_context, sizeof(G_password_context));

    //Initialize stored passwords storage for the first time
    if (N_savedPasswordsStorage.initialized != 0x01) {
        savedPasswordsStorage_t storage;
        explicit_bzero(&storage, sizeof(savedPasswordsStorage_t));
        storage.initialized = 0x01;
        nvm_write((void *) &N_savedPasswordsStorage, (void *) &storage, sizeof(savedPasswordsStorage_t));

        /// ONLY FOR EMULATOR
        // savedPasswordsStorage_t passwordStorage = N_savedPasswordsStorage;
        // strlcpy(passwordStorage.passwords[passwordStorage.size].website, "www.ledger.com", strlen("www.ledger.com")+1);
        // strlcpy(passwordStorage.passwords[passwordStorage.size].username, "einaras.garbasauskas@gmail.com", strlen("einaras.garbasauskas@gmail.com")+1);

        // uint8_t newEncryptedPassword[32] = {142, 152, 67, 64, 8, 81, 243, 221, 135, 207, 113, 125, 105, 205, 254, 224, 32, 160, 173, 249, 186, 170, 54, 225, 69, 101, 74, 178, 83, 69, 136, 29};
        // memcpy(passwordStorage.passwords[passwordStorage.size].password.passwordBytes, newEncryptedPassword, 32);
        // passwordStorage.passwords[passwordStorage.size].password.size = 32;
        // passwordStorage.size++;

        // strlcpy(passwordStorage.passwords[passwordStorage.size].website, "google.com", strlen("google.com")+1);
        // strlcpy(passwordStorage.passwords[passwordStorage.size].username, "einaras@gmail.com", strlen("einaras@gmail.com")+1);

        // uint8_t newEncryptedPassword2[16] = {71,77,89,191,214,157,64,185,58,250,212,144,133,29,94,81};
        // memcpy(passwordStorage.passwords[passwordStorage.size].password.passwordBytes, newEncryptedPassword2, 16);
        // passwordStorage.passwords[passwordStorage.size].password.size = 16;
        // passwordStorage.size++;

        // strlcpy(passwordStorage.passwords[passwordStorage.size].website, "google2.com", strlen("google2.com")+1);
        // strlcpy(passwordStorage.passwords[passwordStorage.size].username, "einaras2@gmail.com", strlen("einaras2@gmail.com")+1);

        // memcpy(passwordStorage.passwords[passwordStorage.size].password.passwordBytes, newEncryptedPassword2, 16);
        // passwordStorage.passwords[passwordStorage.size].password.size = 16;
        // passwordStorage.size++;
        
        // passwordStorage.initialized = 0x01;

        // nvm_write((void *) &N_savedPasswordsStorage, (void *) &passwordStorage, sizeof(savedPasswordsStorage_t));
        // PRINTF("Initialized %d stored passwords\n", N_savedPasswordsStorage.size);
        // for(int i = 0; i < N_savedPasswordsStorage.size; i++) {
        //     PRINTF("Website: %s\n", N_savedPasswordsStorage.passwords[i].website);
        //     PRINTF("Username: %s\n", N_savedPasswordsStorage.passwords[i].username);
        //     PRINTF("Password: ");
        //     for(int j = 0; j < N_savedPasswordsStorage.passwords[i].password.size; j++) {
        //         PRINTF("%d ", N_savedPasswordsStorage.passwords[i].password.passwordBytes[j]);
        //     }
        //     PRINTF("\n");
        // }
        ///
    }

    for (;;) {
        BEGIN_TRY {
            TRY {
                // Reset structured APDU command
                memset(&cmd, 0, sizeof(cmd));

                // Receive command bytes in G_io_apdu_buffer
                if ((input_len = io_recv_command()) < 0) {
                    return;
                }

                // Parse APDU command from G_io_apdu_buffer
                if (!apdu_parser(&cmd, G_io_apdu_buffer, input_len)) {
                    PRINTF("=> /!\\ BAD LENGTH: %.*H\n", input_len, G_io_apdu_buffer);
                    io_send_sw(SW_WRONG_DATA_LENGTH);
                    continue;
                }

                PRINTF("=> CLA=%02X | INS=%02X | P1=%02X | P2=%02X | Lc=%02X | CData=%.*H\n",
                       cmd.cla,
                       cmd.ins,
                       cmd.p1,
                       cmd.p2,
                       cmd.lc,
                       cmd.lc,
                       cmd.data);

                // Dispatch structured APDU command to handler
                if (apdu_dispatcher(&cmd) < 0) {
                    return;
                }
            }
            CATCH(EXCEPTION_IO_RESET) {
                THROW(EXCEPTION_IO_RESET);
            }
            CATCH_OTHER(e) {
                io_send_sw(e);
            }
            FINALLY {
            }
            END_TRY;
        }
    }
}

/**
 * Exit the application and go back to the dashboard.
 */
void app_exit() {
    BEGIN_TRY_L(exit) {
        TRY_L(exit) {
            os_sched_exit(-1);
        }
        FINALLY_L(exit) {
        }
    }
    END_TRY_L(exit);
}

/**
 * Main loop to setup USB, Bluetooth, UI and launch app_main().
 */
__attribute__((section(".boot"))) int main() {
    __asm volatile("cpsie i");

    os_boot();

    for (;;) {
        // Reset UI
        memset(&G_ux, 0, sizeof(G_ux));

        BEGIN_TRY {
            TRY {
                io_seproxyhal_init();

#ifdef TARGET_NANOX
                G_io_app.plane_mode = os_setting_get(OS_SETTING_PLANEMODE, NULL, 0);
#endif  // TARGET_NANOX

                USB_power(0);
                USB_power(1);

                ui_menu_main();

#ifdef HAVE_BLE
                BLE_power(0, NULL);
                BLE_power(1, "Nano X");
#endif  // HAVE_BLE
                app_main();
            }
            CATCH(EXCEPTION_IO_RESET) {
                CLOSE_TRY;
                continue;
            }
            CATCH_ALL {
                CLOSE_TRY;
                break;
            }
            FINALLY {
            }
        }
        END_TRY;
    }

    app_exit();

    return 0;
}
